const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const webpackMerge = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const loadPresets = require('./build-utils/loadPresets');

/* eslint-disable */
const modeConfig = env => require(`./build-utils/webpack.${env.mode}.js`)(env);
/* eslint-enable */

module.exports = ({ mode, presets }) => webpackMerge({
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
        },
        exclude: /node_modules(?!\/@hc\/common)/,
      },
    ],
  },
  mode,
  resolve: {
    symlinks: true,
  },
  entry: {
    app: './src/index.js',
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebPackPlugin({
      title: 'Client Progressive Web Application',
      filename: 'index.html',
      template: './src/index.html',
      favicon: './src/favicon.ico',
    }),
    new CopyWebpackPlugin(
      [
        { from: 'src/images', to: 'images/' },
        'src/manifest.json',
        'src/sw-client.js',
      ],
      { ignore: ['.DS_Store'] },
    ),
  ],
  output: {
    publicPath: '/',
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
},
modeConfig({ mode, presets }),
loadPresets({ mode, presets }));
