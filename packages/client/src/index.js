import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import App from './containers/App/App';
import ErrorBoundary from './ErrorBoundary';
import installator from './install';

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  </Provider>,
  document.getElementById('root'),
);

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw-client.js').then((registration) => {
      /* eslint-disable-next-line no-console */
      console.info('SW registered: ', registration);
    }).catch((registrationError) => {
      /* eslint-disable-next-line no-console */
      console.error('SW registration failed: ', registrationError);
    });
  });
}
installator();
