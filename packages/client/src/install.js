
let deferredInstallPrompt = null;
const installButton = document.getElementById('butInstall');
/* eslint-disable-next-line no-use-before-define */
window.addEventListener('beforeinstallprompt', saveBeforeInstallPromptEvent);

/* eslint-disable-next-line import/prefer-default-export */
export default function install() {
  /* eslint-disable-next-line no-use-before-define */
  installButton.addEventListener('click', installPWA);
}

/**
 * Event handler for beforeinstallprompt event.
 *   Saves the event & shows install button.
 *
 * @param {Event} evt
 */
function saveBeforeInstallPromptEvent(evt) {
  deferredInstallPrompt = evt;
  installButton.removeAttribute('hidden');
}

/**
 * Event handler for butInstall - Does the PWA installation.
 *
 * @param {Event} evt
 */
function installPWA(evt) {
  // Add code show install prompt & hide the install button.
  deferredInstallPrompt.prompt();
  // Hide the install button, it can't be called twice.
  evt.srcElement.setAttribute('hidden', true);

  // Log user response to prompt.
  deferredInstallPrompt.userChoice
    .then((choice) => {
      /* eslint-disable no-console */
      if (choice.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt', choice);
      } else {
        console.log('User dismissed the A2HS prompt', choice);
      }
      /* eslint-enable no-console */
      deferredInstallPrompt = null;
    });
}

// Add event listener for appinstalled event
/* eslint-disable-next-line no-use-before-define */
window.addEventListener('appinstalled', logAppInstalled);


/**
 * Event handler for appinstalled event.
 *   Log the installation to analytics or save the event somehow.
 *
 * @param {Event} evt
 */
function logAppInstalled(evt) {
  /* eslint-disable-next-line no-console */
  console.info('App was installed.', evt);
}
