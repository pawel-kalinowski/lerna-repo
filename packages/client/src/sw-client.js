/* eslint-disable no-undef */
importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

/* eslint-disable-next-line no-console */
console.info('service worker running');

workbox.core.setCacheNameDetails({
  prefix: 'hottCoffee',
  suffix: 'v1',
  precache: 'hc',
});

workbox.precaching.precacheAndRoute([
  { url: '/favicon.ico', revision: 'v1' },
  { url: '/manifest.json', revision: 'v1' },
  { url: '/app.bundle.js', revision: 'v1' },
  { url: '/index.html', revision: 'v1' },
]);
