import React from 'react';
import PropTypes from 'prop-types';
import './FlashMessage.scss';

const FlashMessage = ({ message }) => (
  <>
    {message && (
      <div className="error-message">
        <span>{`An error occurred: ${message}`}</span>
      </div>
    )}
  </>
);

FlashMessage.propTypes = {
  message: PropTypes.string,
};

FlashMessage.defaultProps = {
  message: '',
};

export default FlashMessage;
