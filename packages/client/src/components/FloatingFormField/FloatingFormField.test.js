import React from 'react';
import { shallow } from 'enzyme';

import FloatingFormField from './FloatingFormField';

describe('FloatingFormField suit test', () => {
  it('FloatingFormField component render properly with correct props', () => {
    const onChange = () => true;
    const name = 'name';
    const id = 'id';
    const wrapper = shallow(
      <FloatingFormField
        id={id}
        label="label"
        name={name}
        onChange={onChange}
      />,
    );
    expect(wrapper.find('input').props().name).toBe(name);
    expect(wrapper.find('input').props().id).toBe(id);
    expect(wrapper.find('input').props().onChange).toBe(onChange);
  });
});
