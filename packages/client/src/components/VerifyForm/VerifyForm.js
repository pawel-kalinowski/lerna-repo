import React from 'react';
import PropTypes from 'prop-types';
import FloatingFormField from '../FloatingFormField';
import './VerifyForm.scss';

const VerifyForm = ({ handleSubmit, handleFields, code }) => (
  <div className="form verify-form">
    <form onSubmit={handleSubmit}>
      <FloatingFormField
        onChange={handleFields}
        name="code"
        id="code"
        label="Verification code"
        value={code}
      />
      <div className="btn-wrapper">
        <button className="submit btn-primary" type="submit">Continue</button>
      </div>
    </form>
  </div>
);


VerifyForm.propTypes = {
  code: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleFields: PropTypes.func.isRequired,
};

export default VerifyForm;
