import React from 'react';
import { shallow } from 'enzyme';

import RadioFormField from './RadioFormField';

describe('RadioFormField suit test', () => {
  it('RadioFormField component render properly with correct props', () => {
    const name = 'name';
    const id = 'id';
    const onChange = () => true;
    const wrapper = shallow(
      <RadioFormField
        checked
        label="label"
        name={name}
        id={id}
        onChange={onChange}
      />,
    );
    expect(wrapper.find('input').props().name).toBe(name);
    expect(wrapper.find('input').props().id).toBe(id);
    expect(wrapper.find('input').props().onChange).toBe(onChange);
  });
});
