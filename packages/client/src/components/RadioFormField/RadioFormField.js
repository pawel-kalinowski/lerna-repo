import React from 'react';
import PropTypes from 'prop-types';
import './RadioFormField.scss';

const RadioFormField = ({
  name,
  id,
  value,
  onChange,
  checked,
  label,
}) => (
  <div className="input-radio">
    <input
      type="radio"
      name={name}
      id={id}
      value={value}
      onChange={onChange}
      checked={checked}
    />
    <label htmlFor={id}>{label}</label>
  </div>
);

RadioFormField.propTypes = {
  checked: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

RadioFormField.defaultProps = {
  value: null,
};

export default RadioFormField;
