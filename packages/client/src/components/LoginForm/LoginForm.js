import React from 'react';
import PropTypes from 'prop-types';
import FloatingFormField from '../FloatingFormField';
import './LoginForm.scss';

const LoginForm = ({ handleSubmit, handleFields, email, password }) => (
  <div className="form login-form">
    <form onSubmit={handleSubmit}>
      <FloatingFormField
        onChange={handleFields}
        name="email"
        id="email"
        label="Login"
        value={email}
      />
      <FloatingFormField
        onChange={handleFields}
        name="password"
        id="password"
        label="Password"
        value={password}
        type="password"
      />
      <div className="btn-wrapper">
        <button className="btn-primary" type="submit">Sign in</button>
      </div>
    </form>
  </div>
);

LoginForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  handleFields: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default LoginForm;
