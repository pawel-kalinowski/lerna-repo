import React from 'react';
import PropTypes from 'prop-types';
import SelectFormField from '@hc/common/src/components/SelectFormField';
import VerificationTypeEnum from '../../utils/VerificationTypeEnum';
import countriesPhoneCodes from '../../utils/CountriesPhoneCodes';
import FloatingFormField from '../FloatingFormField';
import RadioFormField from '../RadioFormField';
import './RegisterForm.scss';

const RegisterForm = ({
  handleSubmit,
  handleFields,
  email,
  password,
  name,
  phone,
  countryCode,
  verifiedBy,
}) => (
  <div className="form registration-form">
    <form onSubmit={handleSubmit}>
      <div className="row">
        <FloatingFormField
          onChange={handleFields}
          name="email"
          id="email"
          label="Login"
          value={email}
        />
      </div>
      <div className="row">
        <FloatingFormField
          onChange={handleFields}
          name="password"
          id="password"
          label="Password"
          value={password}
          type="password"
        />
      </div>
      <div className="row">
        <FloatingFormField
          onChange={handleFields}
          name="name"
          id="name"
          label="Name"
          value={name}
        />
      </div>
      <div className="row">
        <FloatingFormField
          onChange={handleFields}
          name="phone"
          id="phone"
          label="Phone"
          value={phone}
        />
      </div>
      <div className="row select-wrapper">
        <SelectFormField
          label="Country code"
          name="countryCode"
          id="countryCode"
          value={countryCode}
          onChange={handleFields}
          options={countriesPhoneCodes}
          optionLabelFormatter={country => `${country.shortName}/${country.name} ${country.code}`}
          optionKey={country => country.shortName}
          optionValue={country => country.code}
        />
      </div>
      <div className="input-radio-container">
        <label htmlFor="verifiedBy">
          Verify by:
        </label>
        <div className="input-radio-wrapper">
          <RadioFormField
            name="verifiedBy"
            id="emailVerify"
            value={VerificationTypeEnum.EMAIL}
            onChange={handleFields}
            checked={verifiedBy === VerificationTypeEnum.EMAIL}
            label="Email"
          />
          <RadioFormField
            name="verifiedBy"
            id="mobileVerify"
            value={VerificationTypeEnum.MOBILE}
            onChange={handleFields}
            checked={verifiedBy === VerificationTypeEnum.MOBILE}
            label="Mobile"
          />
        </div>
      </div>
      <div className="btn-wrapper">
        <button className="btn-primary" type="submit">Sign in</button>
      </div>
    </form>
  </div>
);

RegisterForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  phone: PropTypes.string,
  countryCode: PropTypes.string.isRequired,
  verifiedBy: PropTypes.string.isRequired,
  handleFields: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

RegisterForm.defaultProps = {
  phone: '',
};

export default RegisterForm;
