import React from 'react';
import { shallow } from 'enzyme';

import SelectFormField from '@hc/common/src/components/SelectFormField';
import RegisterForm from './RegisterForm';
import FloatingFormField from '../FloatingFormField';
import RadioFormField from '../RadioFormField';

describe('Register form suit test', () => {
  it('Register form render properly', () => {
    const wrapper = shallow(
      <RegisterForm
        name=""
        email=""
        verifiedBy="1"
        phone=""
        password=""
        countryCode=""
        handleFields={() => {}}
        handleSubmit={() => {}}
      />,
    );
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find(FloatingFormField)).toHaveLength(4);
    expect(wrapper.find(SelectFormField)).toHaveLength(1);
    expect(wrapper.find(RadioFormField)).toHaveLength(2);
    expect(wrapper.find('button')).toHaveLength(1);
  });
});
