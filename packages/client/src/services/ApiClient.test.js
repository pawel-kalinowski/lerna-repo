import mockAxios from 'axios';
import ApiClient from './ApiClient';
import * as Fixtures from '../__fixtures__/apiClient';

jest.mock('axios');
describe('Test suite for api client service', () => {
  it('should call client login endpoint properly and return proper data object', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.resolve({
      data: Fixtures.validClientWithToken,
    }));

    const response = await ApiClient.signIn('example@email.com', 'example-password');
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });

  it('client login endpoint should return validation error', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.reject(Fixtures.signInErrorResponse));

    const response = await ApiClient.signIn('example@email.com', 'example-password');
    expect(response).toMatchObject({ error: true, message: 'Login should be valid email' });
  });

  it('client registration endpoint should return token and user object', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.resolve({
      data: Fixtures.validClientWithToken,
    }));

    const response = await ApiClient.signUp(Fixtures.signUpData);
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });

  it('client registration endpoint should return token and user object', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.resolve({
      data: Fixtures.validClientWithToken,
    }));

    const response = await ApiClient.signUp(Fixtures.signUpData);
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });

  it('client registration endpoint should return array with validation errors', async () => {
    mockAxios.post.mockImplementationOnce(
      () => Promise.reject(Fixtures.signUpResponseValidationErrors),
    );

    const response = await ApiClient.signUp(Fixtures.signUpData);
    expect(response).toMatchObject(
      { error: true, message: Fixtures.signUpValidationErrors },
    );
  });

  it('verify endpoint should return token and user object', async () => {
    mockAxios.post.mockImplementationOnce(
      () => Promise.resolve({ data: Fixtures.validClientWithToken }),
    );

    const response = await ApiClient.verifyCode('1234', 'example@email.com');
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });
});
