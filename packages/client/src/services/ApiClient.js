import api from '../utils/api.config';
import ErrorWrapperHelper from '../helpers/ErrorWrapperHelper';

export default class ApiClient {
  static signIn = async (email, password) => {
    try {
      const response = await api.post('/clients/sign-in', { email, password });
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  };

  static signUp = async (data) => {
    try {
      const response = await api.post('clients', data);
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  };

  static verifyCode = async (code, email) => {
    try {
      const response = await api.post('/clients/verify', { code, email });
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  };

  static resendVerificationCode = async (email) => {
    try {
      const response = await api.post('/clients/verify/resend', email);
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  }
}
