export default (data) => {
  const fields = {
    email: data.email,
    password: data.password,
    name: data.name,
    verifiedBy: data.verifiedBy,
  };
  if (data.phone !== '') {
    fields.phone = data.phone;
  }
  if (data.countryCode !== '') {
    fields.countryCode = data.countryCode;
  }
  return fields;
};
