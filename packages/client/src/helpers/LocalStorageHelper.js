export default class LocalStorageHelper {
  static createRecord = (name, value) => {
    try {
      localStorage.setItem(name, value);
    } catch (exception) {
      throw new Error(exception);
    }
  };

  static loadRecord = (name) => {
    try {
      return localStorage.getItem(name);
    } catch (exception) {
      throw new Error(exception);
    }
  };

  static deleteRecord = (name) => {
    try {
      localStorage.removeItem(name);
    } catch (exception) {
      throw new Error(exception);
    }
  };
}
