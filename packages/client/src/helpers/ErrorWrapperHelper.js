const ErrorWrapperHelper = (error) => {
  if (error.response) {
    return {
      error: true,
      message: error.response.data.errors
        ? error.response.data.errors : error.response.data.message,
    };
  }
  return {
    error: true,
    message: error.message,
  };
};

export default ErrorWrapperHelper;
