import mapper from './mapStateToFields';

describe('Map data to fields test case', () => {
  it('Should return proper fields object without phone and country code', () => {
    const input = {
      email: 'example@email.com',
      name: 'example',
      password: '1234567890',
      verifiedBy: '1',
    };

    const output = {
      ...input,
    };
    expect(mapper(input)).toEqual(output);
  });

  it('Should return proper fields object with all fields', () => {
    const input = {
      email: 'example@email.com',
      name: 'example',
      password: '1234567890',
      phone: '1234567890',
      countryCode: '78',
      verifiedBy: '1',
    };

    const output = {
      ...input,
    };
    expect(mapper(input)).toEqual(output);
  });
});
