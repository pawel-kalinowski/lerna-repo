import React from 'react';
import { shallow } from 'enzyme/build';
import Verify from './Verify';

describe('Verify container suit test', () => {
  test('Verify container rendered properly', () => {
    const wrapper = shallow(
      <Verify email="example@email.com" auth={() => {}} />,
    );

    expect(wrapper.find('.container')).toHaveLength(1);
  });
});
