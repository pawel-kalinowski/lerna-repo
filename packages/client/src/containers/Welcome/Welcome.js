import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import LocalSession from '../../utils/LocalSession';
import {
  ROUTE_LOGIN, ROUTE_REGISTER, ROUTE_HOME,
} from '../../utils/Routes';

class Welcome extends Component {
  componentDidMount = async () => {
    const data = {
      token: LocalSession.getSession('token') === 'null' ? null : LocalSession.getSession('token'),
      email: LocalSession.getSession('email'),
    };
    this.props.auth(data);
  };

  render() {
    return (
      <>
        {this.props.token && <Redirect to={ROUTE_HOME} />}
        <div className="container">
          <div className="logo" />
          <div className="navigation">
            <div className="info">Welcome to client app</div>
            <Link to={ROUTE_LOGIN} className="btn-primary">Login</Link>
            <Link to={ROUTE_REGISTER} className="btn-primary">Register</Link>
            <a href={process.env.ESCORT_URL} className="btn-primary">Go to Escort app</a>
          </div>
        </div>
      </>
    );
  }
}

Welcome.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
};

Welcome.defaultProps = {
  token: null,
};

export default Welcome;
