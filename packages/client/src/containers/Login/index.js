import { connect } from 'react-redux';
import Login from './Login';
import Auth from '../../actions/Auth';

const mapStateToProps = data => ({
  token: data.auth.token,
  email: data.auth.email,
});
const mapDispatchToProps = dispatch => ({
  auth: data => dispatch(Auth(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
