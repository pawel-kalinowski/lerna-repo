const signUpData = {
  email: 'example@email.com',
  phone: '1234567890',
  name: 'exampleName',
  countryCode: 'EN',
  verifiedBy: '1',
  password: 'password',
};

const validClientWithToken = {
  data: {
    token: 'example-token',
    user: {
      email: 'example@email.com',
      name: 'exampleName',
    },
  },
};

const signInErrorResponse = {
  response: {
    data: {
      message: 'Login should be valid email',
    },
  },
};

const signUpValidationErrors = [
  {
    message: 'Phone is required',
    field: 'phone',
  },
  {
    message: 'Name is required',
    field: 'name',
  },
  {
    message: 'Email is required',
    field: 'email',
  },
  {
    message: 'Password is required',
    field: 'password',
  },
];

const signUpResponseValidationErrors = {
  response: {
    data: {
      errors: signUpValidationErrors,
    },
  },
};

export {
  signUpData,
  signInErrorResponse,
  validClientWithToken,
  signUpValidationErrors,
  signUpResponseValidationErrors,
};
