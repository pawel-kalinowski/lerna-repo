module.exports = {
  presets: [
    ['@babel/preset-env',
      {
        useBuiltIns: 'usage',
        debug: false,
        corejs: '3.0.0',
        targets: {
          esmodules: true,
          ie: '11',
        },
      }],
    '@babel/preset-react'],
  env: {
    production: {
      plugins: [
        '@babel/plugin-proposal-class-properties',
        'transform-react-remove-prop-types', {
          mode: 'remove',
          ignoreFilenames: ['node_modules'],
        }],
    },
  },
  plugins: [
    '@babel/plugin-proposal-class-properties',
  ],
};
