const DEVELOPMENT_MODE = 'development';
const PRODUCTION_MODE = 'production';

export default {
  DEVELOPMENT_MODE,
  PRODUCTION_MODE,
};
