import React from 'react';
import { shallow } from 'enzyme';

import SelectFormField from './SelectFormField';

describe('SelectFormField suit test', () => {
  it('SelectFormField component render properly with correct props', () => {
    const onChange = () => true;
    const name = 'name';
    const id = 'id';
    const optionValue = 'value';
    const options = [{ name: 'name', value: optionValue }];
    const optionValueFunction = item => item.value;
    const wrapper = shallow(
      <SelectFormField
        id={id}
        label="label"
        name={name}
        onChange={onChange}
        optionKey={optionValueFunction}
        optionLabelFormatter={optionValueFunction}
        options={options}
        optionValue={optionValueFunction}
      />,
    );
    expect(wrapper.find('select').props().name).toBe(name);
    expect(wrapper.find('select').props().id).toBe(id);
    expect(wrapper.find('option')).toHaveLength(2);
    expect(wrapper.find(`option[value="${optionValue}"]`)).toHaveLength(1);
    expect(wrapper.find('option').at(1).text(optionValue)).toBe(optionValue);
  });
});
