// TODO server will be changed to NgNix after HC-214

/* eslint-disable-next-line import/no-extraneous-dependencies */
const express = require('express');
const path = require('path');
const { redirectToHTTPS } = require('express-http-to-https');
const cors = require('cors');

const app = express();

app.use(express.static(path.join(__dirname, 'dist')));
app.use(redirectToHTTPS([/localhost:(\d{4})/], [/\/insecure/], 301));
app.use(cors());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

app.listen(9001, () => {
  /* eslint-disable-next-line no-console */
  console.log('Local DevServer Started port 9001...');
});
