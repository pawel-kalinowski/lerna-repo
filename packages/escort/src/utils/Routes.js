const ROUTE_WELCOME = '/';
const ROUTE_HOME = '/home';
const ROUTE_REGISTER = '/register';
const ROUTE_LOGIN = '/login';
const ROUTE_VERIFY = '/verify';
const ROUTE_PERSONAL_INFORMATION = '/personal-information';

export {
  ROUTE_WELCOME,
  ROUTE_REGISTER,
  ROUTE_LOGIN,
  ROUTE_VERIFY,
  ROUTE_HOME,
  ROUTE_PERSONAL_INFORMATION,
};
