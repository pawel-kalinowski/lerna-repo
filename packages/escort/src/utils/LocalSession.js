import LocalStorageHelper from '../helpers/LocalStorageHelper';

const SESSION_PREFIX = 'HOTT_COFFEE_ESCORT';

class LocalSession {
  static getSession = name => LocalStorageHelper.loadRecord(`${SESSION_PREFIX}_${name}`);

  static setSession = (name, value) => LocalStorageHelper.createRecord(`${SESSION_PREFIX}_${name}`, value);

  static deleteSession = name => LocalStorageHelper.deleteRecord(`${SESSION_PREFIX}_${name}`);
}

export default LocalSession;
