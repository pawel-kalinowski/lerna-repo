import React from 'react';
import PropTypes from 'prop-types';
import FloatingFormField from '../FloatingFormField';
import PrimaryButton from '../PrimaryButton';
import styles from './VerifyForm.scss';

const VerifyForm = ({ handleSubmit, handleFields, code }) => (
  <form onSubmit={handleSubmit} className={styles.verifyForm}>
    <FloatingFormField
      onChange={handleFields}
      name="code"
      id="code"
      label="Verification code"
      value={code}
    />
    <PrimaryButton text="Continue" onClick={handleSubmit} />
  </form>
);


VerifyForm.propTypes = {
  code: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleFields: PropTypes.func.isRequired,
};

export default VerifyForm;
