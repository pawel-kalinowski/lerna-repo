import React from 'react';
import { shallow } from 'enzyme';

import VerifyForm from './VerifyForm';
import FloatingFormField from '../FloatingFormField';
import PrimaryButton from '../PrimaryButton/index';

describe('Verify form suit test', () => {
  it('Verify form render properly', () => {
    const wrapper = shallow(
      <VerifyForm
        code=""
        handleFields={() => {}}
        handleSubmit={() => {}}
      />,
    );
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find(FloatingFormField)).toHaveLength(1);
    expect(wrapper.find(PrimaryButton)).toHaveLength(1);
  });
});
