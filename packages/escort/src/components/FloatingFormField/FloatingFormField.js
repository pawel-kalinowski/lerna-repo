import React from 'react';
import PropTypes from 'prop-types';
import styles from './FloatingFormField.scss';

const FloatingFormField = ({
  type,
  name,
  id,
  value,
  onChange,
  label,
}) => (
  <div className={styles.inputFloatingWrapper}>
    <input
      type={type}
      name={name}
      id={id}
      value={value}
      onChange={onChange}
      placeholder=" "
    />
    <label htmlFor={id}>{label}</label>
  </div>
);

FloatingFormField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  value: PropTypes.string,
};

FloatingFormField.defaultProps = {
  value: null,
  type: 'text',
};

export default FloatingFormField;
