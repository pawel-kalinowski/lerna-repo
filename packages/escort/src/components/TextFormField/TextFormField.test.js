import React from 'react';
import { shallow } from 'enzyme';

import TextFormField from './TextFormField';

describe('TextFormField suit test', () => {
  it('TextFormField component render properly with correct props', () => {
    const onChange = () => true;
    const name = 'name';
    const id = 'id';
    const wrapper = shallow(
      <TextFormField
        id={id}
        label="label"
        name={name}
        onChange={onChange}
      />,
    );
    expect(wrapper.find('textarea').props().name).toBe(name);
    expect(wrapper.find('textarea').props().id).toBe(id);
    expect(wrapper.find('textarea').props().onChange).toBe(onChange);
  });
});
