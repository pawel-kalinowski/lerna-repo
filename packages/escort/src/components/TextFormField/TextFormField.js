import React from 'react';
import PropTypes from 'prop-types';
import styles from './TextFormField.scss';

const TextFormField = ({
  name,
  id,
  value,
  onChange,
  label,
}) => (
  <div className={styles.textFormWrapper}>
    <label htmlFor={id}>{label}</label>
    <textarea
      className={styles.textFormField}
      name={name}
      id={id}
      value={value}
      onChange={onChange}
    />
  </div>
);

TextFormField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

TextFormField.defaultProps = {
  value: null,
};

export default TextFormField;
