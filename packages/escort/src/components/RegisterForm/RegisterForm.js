import React from 'react';
import PropTypes from 'prop-types';
import VerificationTypeEnum from '../../utils/VerificationTypeEnum';
import countriesPhoneCodes from '../../utils/CountriesPhoneCodes';
import FloatingFormField from '../FloatingFormField';
import RadioFormField from '../RadioFormField';
import SelectFormField from '../SelectFormField';
import PrimaryButton from '../PrimaryButton';
import styles from './RegisterForm.scss';

const RegisterForm = ({
  handleSubmit,
  handleFields,
  email,
  password,
  name,
  phone,
  countryCode,
  verifiedBy,
}) => (
  <form onSubmit={handleSubmit} className={styles.registerForm}>
    <FloatingFormField
      onChange={handleFields}
      name="email"
      id="email"
      label="Login"
      value={email}
    />
    <FloatingFormField
      onChange={handleFields}
      name="password"
      id="password"
      label="Password"
      value={password}
      type="password"
    />
    <FloatingFormField
      onChange={handleFields}
      name="name"
      id="name"
      label="Name"
      value={name}
    />
    <FloatingFormField
      onChange={handleFields}
      name="phone"
      id="phone"
      label="Phone"
      value={phone}
    />
    <SelectFormField
      label="Country code"
      name="countryCode"
      id="countryCode"
      value={countryCode}
      onChange={handleFields}
      options={countriesPhoneCodes}
      optionLabelFormatter={country => `${country.shortName}/${country.name} ${country.code}`}
      optionKey={country => country.shortName}
      optionValue={country => country.code}
    />
    <div className={styles.inputRadioContainer}>
      <label htmlFor="verifiedBy">
        Verify by:
      </label>
      <div>
        <RadioFormField
          name="verifiedBy"
          id="emailVerify"
          value={VerificationTypeEnum.EMAIL}
          onChange={handleFields}
          checked={verifiedBy === VerificationTypeEnum.EMAIL}
          label="Email"
        />
        <RadioFormField
          name="verifiedBy"
          id="mobileVerify"
          value={VerificationTypeEnum.MOBILE}
          onChange={handleFields}
          checked={verifiedBy === VerificationTypeEnum.MOBILE}
          label="Mobile"
        />
      </div>
    </div>
    <div className={styles.btnWrapper}>
      <PrimaryButton text="Sign in" onClick={handleSubmit} />
    </div>
  </form>
);

RegisterForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  phone: PropTypes.string,
  countryCode: PropTypes.string.isRequired,
  verifiedBy: PropTypes.string.isRequired,
  handleFields: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

RegisterForm.defaultProps = {
  phone: '',
};

export default RegisterForm;
