import React from 'react';
import { shallow } from 'enzyme';
import Logo from './Logo';

describe('Logo suit test', () => {
  it('Logo component render properly', () => {
    const wrapper = shallow(
      <Logo />,
    );
    expect(wrapper).toHaveLength(1);
  });
});
