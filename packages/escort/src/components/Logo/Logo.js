import React from 'react';
import styles from './Logo.scss';

const Logo = () => (
  <div className={styles.logo} />
);

export default Logo;
