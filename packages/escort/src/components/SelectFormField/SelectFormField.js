import React from 'react';
import PropTypes from 'prop-types';
import styles from './SelectFormField.scss';

const SelectFormField = ({
  id,
  label,
  name,
  onChange,
  optionKey,
  optionLabelFormatter,
  options,
  optionValue,
  value,
}) => (
  <div className={styles.selectWrapper}>
    <label htmlFor={id}>{label}</label>
    <select className={styles.selectInput} id={id} name={name} value={value} onChange={onChange}>
      <option value="">Select</option>
      {options.map(option => (
        <option value={optionValue(option)} key={optionKey(option)}>
          {optionLabelFormatter(option)}
        </option>
      ))}
    </select>
  </div>
);

SelectFormField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  optionKey: PropTypes.func.isRequired,
  optionLabelFormatter: PropTypes.func,
  options: PropTypes.array.isRequired,
  optionValue: PropTypes.func.isRequired,
  value: PropTypes.string,
};

SelectFormField.defaultProps = {
  value: null,
  optionLabelFormatter: option => option.value,
};

export default SelectFormField;
