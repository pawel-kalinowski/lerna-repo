import React from 'react';
import { shallow } from 'enzyme';

import FlashMessage from './FlashMessage';

describe('Flash message suit test', () => {
  it('Flash message component render properly with correct message', () => {
    const message = 'example error message';
    const wrapper = shallow(
      <FlashMessage
        message={message}
      />,
    );
    expect(wrapper.find('span').text()).toEqual(`An error occurred: ${message}`);
  });

  it('Flash message component do not render without message props', () => {
    const wrapper = shallow(
      <FlashMessage
        message=""
      />,
    );
    expect(wrapper.find('span')).toHaveLength(0);
  });
});
