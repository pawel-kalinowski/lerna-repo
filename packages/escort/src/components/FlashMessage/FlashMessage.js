import React from 'react';
import PropTypes from 'prop-types';
import styles from './FlashMessage.scss';

const FlashMessage = ({ message }) => (
  <>
    {message && (
      <div className={styles.errorMessage}>
        <span>{`An error occurred: ${message}`}</span>
      </div>
    )}
  </>
);

FlashMessage.propTypes = {
  message: PropTypes.string,
};

FlashMessage.defaultProps = {
  message: '',
};

export default FlashMessage;
