import React from 'react';
import { shallow } from 'enzyme';

import PersonalInformationForm from './PersonalInformationForm';
import FloatingFormField from '../FloatingFormField';
import RadioFormField from '../RadioFormField';
import SelectFormField from '../SelectFormField';
import TextFormField from '../TextFormField';
import PrimaryButton from '../PrimaryButton';

describe('PersonalInformationForm suit test', () => {
  it('PersonalInformationForm component render properly', () => {
    const someFunc = () => true;
    const someArray = [{ name: 'name', value: 'value' }];
    const profileAttributesValues = {
      gender: someArray,
      orientation: someArray,
      bodyType: someArray,
      height: [{ value: 1 }],
    };
    const wrapper = shallow(
      <PersonalInformationForm
        onChangeFormFields={someFunc}
        handleSubmit={someFunc}
        profileAttributesValues={profileAttributesValues}
      />,
    );
    expect(wrapper.find('form')).toHaveLength(1);
    expect(wrapper.find(FloatingFormField)).toHaveLength(2);
    expect(wrapper.find(RadioFormField)).toHaveLength(2);
    expect(wrapper.find(SelectFormField)).toHaveLength(2);
    expect(wrapper.find(TextFormField)).toHaveLength(1);
    expect(wrapper.find(PrimaryButton)).toHaveLength(1);
  });
});
