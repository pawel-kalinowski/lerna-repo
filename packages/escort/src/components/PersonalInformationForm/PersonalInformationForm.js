import React from 'react';
import PropTypes from 'prop-types';
import styles from './PersonalInformationForm.scss';
import FloatingFormField from '../FloatingFormField';
import RadioFormField from '../RadioFormField';
import SelectFormField from '../SelectFormField';
import PrimaryButton from '../PrimaryButton';
import TextFormField from '../TextFormField';


const optionValueGetter = item => item.value;

// TODO implement redux thunk, suggestion from CR https://kmpgroup.atlassian.net/browse/HTTCFF-19
const PersonalInformationForm = ({
  handleSubmit,
  onChangeFormFields,
  publicName,
  dateOfBirth,
  profileAttributesValues,
  gender,
  orientation,
  bodyType,
  height,
  description,
}) => (
  <form onSubmit={handleSubmit} className={styles.personalInformationForm}>
    <h3 className={styles.sectionName}>Personal information</h3>
    <FloatingFormField
      onChange={onChangeFormFields}
      name="publicName"
      id="publicName"
      label="Public name"
      value={publicName}
    />
    <FloatingFormField
      onChange={onChangeFormFields}
      name="dateOfBirth"
      id="dateOfBirth"
      label="Date of birth"
      value={dateOfBirth}
      type="date"
    />
    <div className={styles.inputRadioContainer}>
      <label htmlFor="gender">
        Gender
      </label>
      <div className={styles.inputRadioWrapper}>
        {profileAttributesValues.gender.map(genderAttr => (
          <RadioFormField
            name="gender"
            value={genderAttr.value}
            id={genderAttr.value}
            onChange={onChangeFormFields}
            checked={genderAttr.value === gender}
            label={genderAttr.name}
            key={genderAttr.value}
          />
        ))}
      </div>
    </div>
    <div className={styles.inputRadioContainer}>
      <label htmlFor="orientation">
        Orientation
      </label>
      <div className={styles.inputRadioWrapper}>
        {profileAttributesValues.orientation.map(orientationAttr => (
          <RadioFormField
            name="orientation"
            value={orientationAttr.value}
            id={orientationAttr.value}
            onChange={onChangeFormFields}
            checked={orientationAttr.value === orientation}
            label={orientationAttr.name}
            key={orientationAttr.value}
          />
        ))}
      </div>
    </div>
    <div className={styles.selectContainer}>
      <SelectFormField
        label="Body type"
        name="bodyType"
        id="bodyType"
        value={bodyType}
        onChange={onChangeFormFields}
        options={profileAttributesValues.bodyType}
        optionLabelFormatter={optionValueGetter}
        optionKey={optionValueGetter}
        optionValue={optionValueGetter}
      />
      <SelectFormField
        label="Height"
        name="height"
        id="height"
        value={height}
        onChange={onChangeFormFields}
        options={profileAttributesValues.height}
        optionLabelFormatter={optionValueGetter}
        optionKey={optionValueGetter}
        optionValue={optionValueGetter}
      />
    </div>
    <TextFormField
      name="description"
      id="description"
      label="About me"
      onChange={onChangeFormFields}
      value={description}
    />
    <div className={styles.btnWrapper}>
      <PrimaryButton text="Continue" onClick={handleSubmit} />
    </div>
  </form>
);

PersonalInformationForm.propTypes = {
  profileAttributesValues: PropTypes.shape({
    gender: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string,
    })).isRequired,
    orientation: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string,
    })).isRequired,
    bodyType: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.string,
    })).isRequired,
    height: PropTypes.arrayOf(PropTypes.shape({
      value: PropTypes.number,
    })).isRequired,
  }),
  gender: PropTypes.string,
  orientation: PropTypes.string,
  height: PropTypes.string,
  bodyType: PropTypes.string,
  publicName: PropTypes.string,
  description: PropTypes.string,
  dateOfBirth: PropTypes.string,
  onChangeFormFields: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

PersonalInformationForm.defaultProps = {
  gender: '',
  orientation: '',
  height: '',
  bodyType: '',
  publicName: '',
  description: '',
  dateOfBirth: '',
  profileAttributesValues: {
    gender: [],
    orientation: [],
    bodyType: [],
    height: [],
  },
};

export default PersonalInformationForm;
