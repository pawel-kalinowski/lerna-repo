import React from 'react';
import PropTypes from 'prop-types';

const PrimaryButton = ({
  text,
  onClick,
}) => (
  <button className="btn-primary" type="submit" onClick={onClick}>{text}</button>
);

PrimaryButton.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default PrimaryButton;
