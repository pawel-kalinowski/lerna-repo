import React from 'react';
import { shallow } from 'enzyme';
import PrimaryButton from './PrimaryButton';

describe('Primary button suit test', () => {
  it('Primary button component render properly with correct text', () => {
    const text = 'Test';
    const wrapper = shallow(
      <PrimaryButton
        text={text}
        onClick={() => {}}
      />,
    );
    expect(wrapper.find('button').text()).toEqual(text);
  });

  it('Primary button component do not render without text props', () => {
    const wrapper = shallow(
      <PrimaryButton
        text=""
        onClick={() => {}}
      />,
    );
    expect(wrapper.find('button').text()).toHaveLength(0);
  });
});
