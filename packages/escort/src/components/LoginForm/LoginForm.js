import React from 'react';
import PropTypes from 'prop-types';
import FloatingFormField from '../FloatingFormField';
import PrimaryButton from '../PrimaryButton';
import styles from './LoginForm.scss';

const LoginForm = ({ handleSubmit, handleFields, email, password }) => (
  <div className={styles.loginFormContainer}>
    <form onSubmit={handleSubmit} className={styles.loginForm}>
      <FloatingFormField
        onChange={handleFields}
        name="email"
        id="email"
        label="Login"
        value={email}
      />
      <FloatingFormField
        onChange={handleFields}
        name="password"
        id="password"
        label="Password"
        value={password}
        type="password"
      />
      <div className={styles.btnWrapper}>
        <PrimaryButton text="Sign in" onClick={handleSubmit} />
      </div>
    </form>
  </div>
);

LoginForm.propTypes = {
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  handleFields: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default LoginForm;
