import React from 'react';
import { shallow } from 'enzyme';

import LoginForm from './LoginForm';
import FloatingFormField from '../FloatingFormField';
import PrimaryButton from '../PrimaryButton';

describe('Login form suit test', () => {
  it('Login form render properly with all fields', () => {
    const wrapper = shallow(<LoginForm
      login=""
      password=""
      handleFields={() => {}}
      handleSubmit={() => {}}
      email=""
    />);
    expect(wrapper.find(FloatingFormField)).toHaveLength(2);
    expect(wrapper.find(PrimaryButton)).toHaveLength(1);
  });
});
