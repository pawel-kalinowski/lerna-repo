export default (state = { token: null, email: null }, action) => {
  switch (action.type) {
  case 'AUTH':
    return { ...action.data };
  default:
    return state;
  }
};
