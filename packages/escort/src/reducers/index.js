import { combineReducers } from 'redux';
import auth from './Authentication';

export default combineReducers({
  auth,
});
