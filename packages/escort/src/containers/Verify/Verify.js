import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import ApiEscort from '../../services/ApiEscort';
import VerifyForm from '../../components/VerifyForm/VerifyForm';
import LocalSession from '../../utils/LocalSession';
import FlashMessage from '../../components/FlashMessage/FlashMessage';
import {
  ROUTE_LOGIN, ROUTE_PERSONAL_INFORMATION,
} from '../../utils/Routes';
import styles from './Verify.scss';

class Verify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      errorMessage: '',
    };
  }

  handleFields = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleReSendVerificationCode = async (e) => {
    // TODO feature will be continued by story about resending code
    e.preventDefault();
    const data = await ApiEscort.resendVerificationCode(this.props.email);
    this.setState({
      errorMessage: !data.error ? 'Code has been sent' : data.message,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    const data = await ApiEscort.verifyCode(this.state.code, this.props.email);
    if (!data.error) {
      LocalSession.setSession('token', data.token);
      LocalSession.setSession('email', data.user.email);
      this.props.auth({ token: data.token, email: data.user.email });
    } else {
      this.setState({
        errorMessage: data.message,
      });
    }
  };

  render() {
    return (
      <>
        {(this.props.token && this.props.email) && <Redirect to={ROUTE_PERSONAL_INFORMATION} />}
        {(!this.props.email) && <Redirect to={ROUTE_LOGIN} />}
        <div className={`container ${styles.verifyView}`}>
          <VerifyForm
            {...this.state}
            email={this.props.email}
            token={this.props.token}
            handleFields={this.handleFields}
            handleSubmit={this.handleSubmit}
            handleReSendVerificationCode={this.handleReSendVerificationCode}
          />
          {this.state.errorMessage && <FlashMessage message={this.state.errorMessage} />}

        </div>
      </>
    );
  }
}

Verify.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
  email: PropTypes.string,
};

Verify.defaultProps = {
  token: null,
  email: null,
};

export default Verify;
