import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import LocalSession from '../../utils/LocalSession';
import {
  ROUTE_WELCOME,
} from '../../utils/Routes';
import PrimaryButton from '../../components/PrimaryButton';
import styles from './Home.scss';

class Home extends Component {
  handleSignOut = () => {
    this.props.auth({
      token: null,
      email: null,
    });
    LocalSession.deleteSession('token');
    LocalSession.deleteSession('email');
  };

  componentDidMount = () => {
    const data = {
      token: LocalSession.getSession('token') === 'null' ? null : LocalSession.getSession('token'),
      email: LocalSession.getSession('email'),
    };
    this.props.auth(data);
  };

  render() {
    return (
      <div className={styles.home}>
        {!this.props.token && <Redirect to={ROUTE_WELCOME} />}
        <h3 className={styles.sectionName}>{`Hello ${this.props.email}`}</h3>
        <div className={styles.btnWrapper}>
          <PrimaryButton text="Sign Out" onClick={this.handleSignOut} />
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
  email: PropTypes.string,
};

Home.defaultProps = {
  token: null,
  email: null,
};

export default Home;
