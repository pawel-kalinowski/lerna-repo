import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Auth from '../../actions/Auth';
import LocalSession from '../../utils/LocalSession';
import { ROUTE_LOGIN } from '../../utils/Routes';

class IsLogged extends React.Component {
  componentDidMount = () => {
    const data = {
      token: LocalSession.getSession('token') === 'null' ? null : LocalSession.getSession('token'),
      email: LocalSession.getSession('email'),
    };

    this.props.auth(data);
  };

  render() {
    const { component: Component, ...props } = this.props;
    return (
      <Route
        {...props}
        render={childProps => (
          (this.props.token && this.props.email)
            ? <Component {...childProps} />
            : <Redirect to={ROUTE_LOGIN} />
        )}
      />
    );
  }
}

const mapStateToProps = data => ({
  token: data.auth.token,
  email: data.auth.email,
});

const mapDispatchToProps = dispatch => ({
  auth: data => dispatch(Auth(data)),
});

IsLogged.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
  email: PropTypes.string,
  component: PropTypes.shape({}).isRequired,
};

IsLogged.defaultProps = {
  token: null,
  email: null,
};

export default connect(mapStateToProps, mapDispatchToProps)(IsLogged);
