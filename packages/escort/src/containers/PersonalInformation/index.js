import { connect } from 'react-redux';
import PersonalInformation from './PersonalInformation';
import Auth from '../../actions/Auth';

// TODO implement redux thunk, suggestion from CR https://kmpgroup.atlassian.net/browse/HTTCFF-19
const mapStateToProps = data => ({
  token: data.auth.token,
  email: data.auth.email,
});
const mapDispatchToProps = dispatch => ({
// TODO implement redux thunk, suggestion from CR https://kmpgroup.atlassian.net/browse/HTTCFF-19
  auth: data => dispatch(Auth(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInformation);
