import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import LocalSession from '../../utils/LocalSession';
import PersonalInformationForm from '../../components/PersonalInformationForm/PersonalInformationForm';
import FlashMessage from '../../components/FlashMessage/FlashMessage';
import {
  ROUTE_WELCOME, ROUTE_HOME,
} from '../../utils/Routes';
import ApiEscort from '../../services/ApiEscort';

class PersonalInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      publicName: '',
      dateOfBirth: '',
      description: '',
      gender: '',
      orientation: '',
      bodyType: '',
      height: '',
      isLoading: false,
      errorMessage: '',
      profileAttributesValues: {
        gender: [],
        orientation: [],
        bodyType: [],
        height: Array(101).fill(0).map((el, index) => ({ value: index + 100 })),
      },
    };
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const {
      profileAttributesValues, errorMessage, isLoading, ...data
    } = this.state;
    const response = await ApiEscort.savePersonalInformation(this.props.token, data);
    if (!response.error) {
      // TODO handle response after save profile details
      this.props.history.push(ROUTE_HOME);
    } else {
      this.setState({
        errorMessage: response.message,
      });
    }
  };

  onChangeFormFields = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  componentDidMount = async () => {
    const data = {
      // TODO implement redux thunk, suggestion from CR https://kmpgroup.atlassian.net/browse/HTTCFF-19
      token: LocalSession.getSession('token') === 'null' ? null : LocalSession.getSession('token'),
      email: LocalSession.getSession('email'),
    };
    this.props.auth(data);
    // TODO implement redux thunk, suggestion from CR https://kmpgroup.atlassian.net/browse/HTTCFF-19
    const profileAttributes = await ApiEscort.getPersonalInformationAttributes(data.token);

    if (!profileAttributes.error) {
      this.setState(prevState => ({
        ...prevState,
        profileAttributesValues: {
          ...profileAttributes,
          height: prevState.profileAttributesValues.height,
        },
        isLoading: false,
      }));
    } else {
      this.setState({
        isLoading: false,
      });
    }
  };

  render() {
    return (
      <div className="container">
        {!this.props.token && <Redirect to={ROUTE_WELCOME} />}
        {this.state.isLoading && <div>... loading</div>}
        {!this.state.isLoading && (
          <PersonalInformationForm
            {...this.state}
            onChangeFormFields={this.onChangeFormFields}
            handleSubmit={this.handleSubmit}
          />
        )}
        {this.state.errorMessage && <FlashMessage message={this.state.errorMessage} /> }
      </div>
    );
  }
}

PersonalInformation.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
  email: PropTypes.string,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

PersonalInformation.defaultProps = {
  token: null,
  email: null,
};

export default PersonalInformation;
