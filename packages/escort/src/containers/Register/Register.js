import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import _map from 'lodash/map';
import _isArray from 'lodash/isArray';

import RegisterForm from '../../components/RegisterForm/RegisterForm';
import ApiEscort from '../../services/ApiEscort';
import VerificationTypeEnum from '../../utils/VerificationTypeEnum';
import FlashMessage from '../../components/FlashMessage/FlashMessage';
import mapStateToFields from '../../helpers/mapStateToFields';
import {
  ROUTE_WELCOME, ROUTE_LOGIN,
} from '../../utils/Routes';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      countryCode: '',
      phone: '',
      verifiedBy: VerificationTypeEnum.EMAIL,
      token: '',
      errorMessage: '',
    };
  }

  handleFields = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    const { token, errorMessage, ...params } = this.state;

    const data = await ApiEscort.signUp(mapStateToFields(params));

    if (!data.error) {
      this.setState({
        token: data.token,
      }, () => {
        this.props.auth({
          token: null,
          email: data.user.email,
        });
      });
    } else {
      this.setState({
        errorMessage: _isArray(data.message)
          ? _map(data.message, element => element.message).join() : data.message,
      });
    }
  };

  render() {
    return (
      <>
        { (this.state.token && this.props.email) && <Redirect to="/verify" /> }
        <div className="container">
          <div className="navigation">
            <Link to={ROUTE_LOGIN} className="link">Login</Link>
            <Link to={ROUTE_WELCOME} className="link">Start</Link>
          </div>
          <RegisterForm
            {...this.state}
            handleFields={this.handleFields}
            handleSubmit={this.handleSubmit}
          />
          { this.state.errorMessage && <FlashMessage message={this.state.errorMessage} /> }
        </div>

      </>
    );
  }
}

Register.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
  email: PropTypes.string,
};

Register.defaultProps = {
  token: null,
  email: null,
};

export default Register;
