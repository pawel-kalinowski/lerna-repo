import React from 'react';
import { shallow } from 'enzyme/build';
import Register from './Register';

describe('Register container suit test', () => {
  it('Register container render registration navigation properly', () => {
    const wrapper = shallow(
      <Register
        auth={() => {}}
      />,
    );

    expect(wrapper.find('.container')).toHaveLength(1);
    expect(wrapper.find('.container .navigation')).toHaveLength(1);
    expect(wrapper.find('.container .navigation .link')).toHaveLength(2);
  });
});
