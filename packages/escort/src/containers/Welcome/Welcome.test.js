import React from 'react';
import { shallow } from 'enzyme/build';
import Welcome from './Welcome';

describe('Welcome container suit test', () => {
  it('Welcome container render navigation properly', () => {
    const wrapper = shallow(
      <Welcome
        auth={() => {}}
      />,
    );
    expect(wrapper.find('.navigation')).toHaveLength(1);
  });
});
