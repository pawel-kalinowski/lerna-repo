import { connect } from 'react-redux';
import Welcome from './Welcome';
import Auth from '../../actions/Auth';

const mapStateToProps = data => ({
  token: data.auth.token,
  email: data.auth.email,
});

const mapDispatchToProps = dispatch => ({
  auth: loginData => dispatch(Auth(loginData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
