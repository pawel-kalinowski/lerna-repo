import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import LocalSession from '../../utils/LocalSession';
import {
  ROUTE_LOGIN, ROUTE_REGISTER, ROUTE_HOME,
} from '../../utils/Routes';
import Logo from '../../components/Logo';

class Welcome extends Component {
  componentDidMount = async () => {
    const data = {
      token: LocalSession.getSession('token') === 'null' ? null : LocalSession.getSession('token'),
      email: LocalSession.getSession('email'),
    };
    this.props.auth(data);
  };

  render() {
    return (
      <>
        {this.props.token && <Redirect to={ROUTE_HOME} />}
        <div className="container">
          <Logo />
          <div className="navigation">
            <div className="info">Welcome to Escort app</div>
            <Link to={ROUTE_LOGIN} className="btn-primary">Login</Link>
            <Link to={ROUTE_REGISTER} className="btn-primary">Register</Link>
            <a href={process.env.CLIENT_URL} className="btn-primary">Go to Client app</a>
          </div>
        </div>
      </>
    );
  }
}

Welcome.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
};

Welcome.defaultProps = {
  token: null,
};


export default Welcome;
