import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import ApiEscort from '../../services/ApiEscort';
import LoginForm from '../../components/LoginForm/LoginForm';
import FlashMessage from '../../components/FlashMessage/FlashMessage';
import LocalSession from '../../utils/LocalSession';
import {
  ROUTE_VERIFY, ROUTE_WELCOME, ROUTE_REGISTER, ROUTE_HOME,
} from '../../utils/Routes';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errorMessage: '',
    };
  }

  handleFields = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    this.setState({
      errorMessage: '',
    });
    const data = await ApiEscort.signIn(this.state.email, this.state.password);
    if (!data.error) {
      LocalSession.setSession('token', data.token);
      LocalSession.setSession('email', data.user.email);
      this.props.auth({ token: data.token, email: data.user.email });
    } else {
      this.setState({
        errorMessage: data.message,
      });
    }
  };

  componentDidMount = () => {
    const data = {
      token: LocalSession.getSession('token') === 'null' ? null : LocalSession.getSession('token'),
      email: LocalSession.getSession('email'),
    };
    this.props.auth(data);
  };

  render() {
    let children = (
      <div className="container">
        <div className="navigation">
          <Link to={ROUTE_REGISTER} className="link">Registration</Link>
          <Link to={ROUTE_WELCOME} className="link">Start</Link>
        </div>
        <LoginForm
          {...this.state}
          handleFields={this.handleFields}
          handleSubmit={this.handleSubmit}
        />
        {this.state.errorMessage && <FlashMessage message={this.state.errorMessage} />}
      </div>
    );

    if (!this.props.token && this.props.email) {
      children = (<Redirect to={ROUTE_VERIFY} />);
    }

    if (this.props.token && this.props.email) {
      children = (<Redirect to={ROUTE_HOME} />);
    }

    return (children);
  }
}

Login.propTypes = {
  auth: PropTypes.func.isRequired,
  token: PropTypes.string,
  email: PropTypes.string,
};

Login.defaultProps = {
  token: null,
  email: null,
};

export default Login;
