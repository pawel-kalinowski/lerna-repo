import React from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';
import IsLogged from '../IsLogged/IsLogged';
import Welcome from '../Welcome';
import Login from '../Login/index';
import Register from '../Register';
import Home from '../Home/index';
import Verify from '../Verify/index';
import PersonalInformation from '../PersonalInformation/index';
import {
  ROUTE_VERIFY, ROUTE_WELCOME, ROUTE_REGISTER, ROUTE_HOME, ROUTE_LOGIN, ROUTE_PERSONAL_INFORMATION,
} from '../../utils/Routes';
// TODD styles as modules will be used after https://kmpgroup.atlassian.net/browse/HC-180,
// right now styles are only for demo
// eslint-disable-next-line
import styles from './App.scss';

const App = () => (
  <HashRouter hashType="noslash">
    <div className="main">
      <Switch>
        <Route exact path={ROUTE_WELCOME} component={Welcome} />
        <Route path={ROUTE_LOGIN} component={Login} />
        <Route path={ROUTE_REGISTER} component={Register} />
        <Route path={ROUTE_VERIFY} component={Verify} />
        <Route path={ROUTE_PERSONAL_INFORMATION} component={PersonalInformation} />
        <IsLogged path={ROUTE_HOME} component={Home} />
      </Switch>
    </div>
  </HashRouter>
);

export default App;
