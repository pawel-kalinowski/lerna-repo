import api from '../utils/api.config';
import ErrorWrapperHelper from '../helpers/ErrorWrapperHelper';

export default class ApiEscort {
  static signIn = async (email, password) => {
    try {
      const response = await api.post('/escorts/sign-in', { email, password });
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  };

  static signUp = async (data) => {
    try {
      const response = await api.post('/escorts', data);
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  };

  static verifyCode = async (code, email) => {
    try {
      const response = await api.post('/escorts/verify', { code, email });
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  };

  static getPersonalInformationAttributes = async (token) => {
    try {
      const response = await api.get(
        '/escorts/profile/personal-information/attributes',
        {
          headers: { Authorization: `Bearer ${token}` },
        },
      );
      return response.data;
    } catch (error) {
      // TODO implement redux thunk, suggestion from CR https://kmpgroup.atlassian.net/browse/HTTCFF-19
      return ErrorWrapperHelper(error);
    }
  };

  static savePersonalInformation = async (token, data) => {
    try {
      const response = await api.post('/escorts/profile/personal-information', { ...data },
        {
          headers: { Authorization: `Bearer ${token}` },
        });
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  };

  static resendVerificationCode = async (email) => {
    try {
      const response = await api.post('/escorts/verify/resend', email);
      return response.data;
    } catch (error) {
      return ErrorWrapperHelper(error);
    }
  }
}
