import mockAxios from 'axios';
import ApiEscort from './ApiEscort';
import * as Fixtures from '../__fixtures__/apiEscort';

jest.mock('axios');
describe('Test suite for api Escort service', () => {
  it('should call Escort login endpoint properly and return proper data object', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.resolve({
      data: Fixtures.validEscortWithToken,
    }));

    const response = await ApiEscort.signIn('example@email.com', 'example-password');
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });

  it('Escort login endpoint should return validation error', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.reject(Fixtures.signInErrorResponse));

    const response = await ApiEscort.signIn('example@email.com', 'example-password');
    expect(response).toMatchObject({ error: true, message: 'Login should be valid email' });
  });

  it('Escort registration endpoint should return token and user object', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.resolve({
      data: Fixtures.validEscortWithToken,
    }));

    const response = await ApiEscort.signUp(Fixtures.signUpData);
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });

  it('Escort registration endpoint should return token and user object', async () => {
    mockAxios.post.mockImplementationOnce(() => Promise.resolve({
      data: Fixtures.validEscortWithToken,
    }));

    const response = await ApiEscort.signUp(Fixtures.signUpData);
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });

  it('Escort registration endpoint should return array with validation errors', async () => {
    mockAxios.post.mockImplementationOnce(
      () => Promise.reject(Fixtures.signUpResponseValidationErrors),
    );

    const response = await ApiEscort.signUp(Fixtures.signUpData);
    expect(response).toMatchObject(
      { error: true, message: Fixtures.signUpValidationErrors },
    );
  });

  it('verify endpoint should return token and user object', async () => {
    mockAxios.post.mockImplementationOnce(
      () => Promise.resolve({ data: Fixtures.validEscortWithToken }),
    );

    const response = await ApiEscort.verifyCode('1234', 'example@email.com');
    expect(response.data).toMatchObject(
      { token: 'example-token', user: { email: 'example@email.com', name: 'exampleName' } },
    );
  });
});
