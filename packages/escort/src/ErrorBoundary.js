import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as Sentry from '@sentry/browser';
import DEVELOPMENT_MODE from '../build-utils/config';

export default class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null, eventId: null };
  }

  componentDidCatch(error, errorInfo) {
    if (process.env.NODE_ENV !== DEVELOPMENT_MODE) {
      Sentry.init({
        dsn: process.env.SENTRY,
      });
      this.setState({ error });
      Sentry.withScope((scope) => {
        scope.setExtras(errorInfo);
        const eventId = Sentry.captureException(error);
        this.setState({ eventId });
      });
    }
  }

  render() {
    if (this.state.error && process.env.NODE_ENV !== DEVELOPMENT_MODE) {
      return (
        <button type="button" style={{}} onClick={() => Sentry.showReportDialog({ eventId: this.state.eventId })}>
          An error occurred
        </button>
      );
    }

    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
};
