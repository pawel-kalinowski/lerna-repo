export default {
  get: jest.fn(() => Promise.resolve({
    data: 'data',
  })),
  post: jest.fn(() => Promise.resolve({
    data: 'data',
  })),
  create: jest.fn(function () {
    return this;
  }),
};
