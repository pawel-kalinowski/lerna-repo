const webpackMerge = require('webpack-merge');

const loadPresets = (env = { presets: [] }) => {
  const presets = env.presets || [];
  const mergedPresets = [].concat(...[presets]);

  /* eslint-disable */
  const mergedConfigs = mergedPresets.map(presetName => require(`./webpack.${presetName}`)(env));
  /* eslint-enable */

  return webpackMerge({}, ...mergedConfigs);
};

module.exports = loadPresets;
