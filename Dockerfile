FROM node:10.16.0

WORKDIR /usr/src/app

COPY package.json ./
COPY ./packages/client/package.json ./packages/client/server.js ./packages/client/
COPY ./packages/escort/package.json ./packages/escort/server.js ./packages/escort/
COPY ./packages/common/package.json ./packages/common/


RUN yarn install --no-cache

COPY . .
